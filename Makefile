
pip_tools:
	pip3 install pip-tools

all_reqs: pip_tools
	pip-compile --verbose --resolver=backtracking requirements.in && pip-sync requirements.txt

run_hooks:
	pre-commit install && pre-commit install --install-hooks && pre-commit run --all-files

flake8:
	flake8 src --inline-quotes '"'

isort:
	isort src

check: isort flake8


pg_dump:
	pg_dump -Fc -U castle -h localhost -p 5432 castle > db_dumps/castle.dump

pg_restore:
	pg_restore --verbose -h localhost -U castle -d castle db_dumps/castle.dump

test_load:
	docker-compose -f src/utils/kafka_logics_async/docker-compose.yml up -d --build
	docker-compose -f docker-compose.yml --profile=metrics --profile=kafka up -d --build
	docker-compose -f docker-compose.yml up -d

run_locust:
	locust -f load_tests/kafka_tests.py
