import asyncio
import json
import logging
import signal

import websockets
from aiokafka import AIOKafkaConsumer
from pydantic_settings import BaseSettings

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
ch = logging.StreamHandler()
ch.setLevel(logging.INFO)
formatter = logging.Formatter("%(levelname)-8s [%(asctime)s] %(name)s[%(funcName)s].%(lineno)d: %(message)s")
ch.setFormatter(formatter)
logger.addHandler(ch)

CLIENTS = []


class Settings(BaseSettings):
    KAFKA_URL: str  # "localhost:29092"
    DB_PASSWORD: str  # "password"
    DB_HOST: str  # "localhost"
    DB_PORT: int  # 5432
    DB_NAME: str  # "castle"
    DB_USER: str  # "castle"

    @property
    def pg_dsn(self) -> str:
        return f"postgresql://{self.DB_USER}:{self.DB_PASSWORD}@{self.DB_HOST}:{self.DB_PORT}/{self.DB_NAME}?prepared_statement_cache_size=0"  # NOQA


settings = Settings()


async def broad_cast_event(data: str):
    results = await asyncio.gather(
        *[ws.send(data) for ws in CLIENTS],
        return_exceptions=True,
    )
    for i, r in enumerate(results):
        if r:
            CLIENTS.pop(i)


async def handler(websocket, path):
    ws_index = len(CLIENTS)
    CLIENTS.append(websocket)
    logger.info("NEW CONENCTION!")
    logger.info(f"amount of clients: {len(CLIENTS)}")
    try:
        while True:
            data = await websocket.recv()
            await broad_cast_event(data=data)
    except websockets.ConnectionClosedOK:
        logger.info("CONNECTION CLOSED OK!")
    except websockets.ConnectionClosedError as e:
        logger.error(e)


def deserializer(serialized):
    return json.loads(serialized)

async def process_message(message):
    print(f"process message start {message['camera_name']}")
    await asyncio.sleep(3)
    message["type"] = "event"
    print(f"process message end {message['camera_name']}")
    return message

async def consume(consumer):
    # consumer will decompress messages automatically
    # in accordance to compression type specified in producer

    data = await consumer.getmany(timeout_ms=10000)
    tasks_process = []
    for _tp, messages in data.items():
        for message in messages:
            # приняли событие по шине, обрабатываем
            tasks_process.append(process_message(message.value))
    logger.info("start processing group of messages")
    tasks_process_results = await asyncio.gather(*tasks_process, return_exceptions=True)
    logger.info(f"end processing group of messages {tasks_process_results=}")
    tasks_send = []
    for m in tasks_process_results:
        tasks_send.append(broad_cast_event(data=json.dumps(m, default=str)))
    logger.info("send to ws")
    # раздаем клиентам вебсокета
    await asyncio.gather(*tasks_send, return_exceptions=True)
    logger.info("sended to ws")


async def start_consuming(consumer):
    await consumer.start()
    logger.info("start consuming")
    try:
        while True:
            await consume(consumer)
    except Exception as e:
        logger.exception(e)
        await consumer.stop()


async def create_consumer():
    return AIOKafkaConsumer(
        "PROCESS_DATA",
        bootstrap_servers=settings.KAFKA_URL,
        value_deserializer=deserializer,
        group_id="backend_consumers",  # Consumer must be in a group to commit
        enable_auto_commit=True,  # Is True by default anyway
        auto_commit_interval_ms=1000,  # Autocommit every second
        auto_offset_reset="earliest",  # If committed offset not found, start from beginning
    )


async def server():
    # Set the stop condition when receiving SIGTERM.
    logger.info("start server")
    loop = asyncio.get_running_loop()
    stop = loop.create_future()
    loop.add_signal_handler(signal.SIGTERM, stop.set_result, None)
    async with websockets.serve(handler, "0.0.0.0", 8080):
        logger.info("server started")
        await stop


if __name__ == "__main__":
    loop = asyncio.new_event_loop()
    consumer = loop.run_until_complete(create_consumer())
    loop.create_task(start_consuming(consumer))
    loop.create_task(server())
    loop.run_forever()
