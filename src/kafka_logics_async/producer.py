import json

from aiokafka import AIOKafkaProducer


def serializer(value):
    return json.dumps(value).encode()


async def produce():
    producer = AIOKafkaProducer(
        bootstrap_servers="localhost:29092",
        value_serializer=serializer,
        compression_type="gzip",
    )

    await producer.start()
