import logging
import os

from confluent_kafka.admin import AdminClient, NewTopic

logging.basicConfig()
logging.root.setLevel(logging.NOTSET)
logger = logging.getLogger(__name__)

logger.info(os.getenv("KAFKA_URL"))

admin_client = AdminClient({"bootstrap.servers": "localhost:29092"})


def create_topics():
    topic_param = [
        ("PROCESS_DATA", 10),
    ]

    new_topics = [
        NewTopic(topic_name, num_partitions=num_partitions, replication_factor=1)
        for topic_name, num_partitions in topic_param
    ]
    current_topics = admin_client.list_topics()
    logger.info(f"{current_topics=}")
    logger.info(f"{current_topics.topics=}")
    # Если нужно удалить топики - deleted_topics = admin_client.delete_topics(list(current_topics.topics.keys()))
    # Выводим информацию об удаленных топиках - logger.info(f"{deleted_topics=}")
    fs = admin_client.create_topics(new_topics)

    for topic, f in fs.items():
        try:
            f.result()  # The result itself is None
            logger.info("Topic {} created".format(topic))  # NOQA
        except Exception as e:
            logger.info("Failed to create topic {}: {}".format(topic, e))  # NOQA


create_topics()
