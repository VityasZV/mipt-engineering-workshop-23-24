import logging
import os
import typing as t

import numpy as np
import prometheus_client as prom

# set logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
ch = logging.StreamHandler()
ch.setLevel(logging.INFO)
formatter = logging.Formatter("%(levelname)-8s [%(asctime)s] %(name)s[%(funcName)s].%(lineno)d: %(message)s")
ch.setFormatter(formatter)
logger.addHandler(ch)


class CastleMetricsHandler:
    """
    class for handling metrics in business logic
    """

    WINDOW_SIZE: t.ClassVar[int] = int(os.getenv("WINDOW_SIZE"))
    EVENTS_PER_SECOND_ON_RECEIVER: t.ClassVar[prom.Gauge] = prom.Gauge(
        "events_per_second_on_receiver",
        "events per second",
    )
    HEARTBEAT_PER_SECOND_ON_RECEIVER: t.ClassVar[prom.Gauge] = prom.Gauge(
        "heartbeat_per_second_on_receiver",
        "heartbeat per second",
    )
    COMMON_CAD: t.ClassVar[prom.Gauge] = prom.Gauge(
        "common_cad",
        "delay from ML to CLIENT",
    )
    KAFKA_NETWORK_CAD: t.ClassVar[prom.Gauge] = prom.Gauge(
        "kafka_network_cad",
        "delay between ML and db_consumer",
    )
    DB_INSERT_CAD: t.ClassVar[prom.Gauge] = prom.Gauge(
        "db_insert_cad",
        "delay for inserting to DB",
    )
    WEB_SOCKET_HEARTBEAT: t.ClassVar[prom.Gauge] = prom.Gauge(
        "web_socket_heartbeat",
        "heartbeat from web socket 1 - ok, 0 - not ok",
    )
    WEB_SOCKET_EVENT: t.ClassVar[prom.Gauge] = prom.Gauge(
        "web_socket_event",
        "event from web socket 1 - ok, 0 - not ok",
    )
    CDF_LATENCY: t.ClassVar[prom.Gauge] = prom.Gauge("cdf_latency", "CDF of delay distribution", ["percentile"])

    @staticmethod
    def get_cummulative_average(
        delta_list: list[float],
    ) -> float:
        """
        calculates cummulative average on list of float values
        """
        cumsum = np.cumsum(np.insert(delta_list, 0, 0))
        window_size = CastleMetricsHandler.WINDOW_SIZE
        return ((cumsum[window_size:] - cumsum[:-window_size]) / window_size)[0]

    @classmethod
    def update_cummulative_average_delay_metric(
        cls,
        delta_list: list[float],
        delta: float,
        metric: prom.Gauge,
    ) -> None:
        if len(delta_list) < cls.WINDOW_SIZE:
            delta_list.insert(0, delta)
            return
        cummulative_average_delay = cls.get_cummulative_average(delta_list)
        metric.set(cummulative_average_delay)
        delta_list.pop()
        delta_list.insert(0, delta)

    @staticmethod
    def heartbeat_and_event_web_socket_metric(
        metric_heartbeat: prom.Gauge,
        metric_event: prom.Gauge,
        heartbeat: t.Literal[1, 0],
        event: t.Literal[1, 0],
    ) -> None:
        metric_heartbeat.set(heartbeat)
        metric_event.set(event)
