import json
import logging
import os
import time
from datetime import datetime

import prometheus_client as prom
from prometheus_client import Counter, Gauge
from metrics.metrics_handler import CastleMetricsHandler
from websocket import WebSocketConnectionClosedException, create_connection

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
ch = logging.StreamHandler()
ch.setLevel(logging.INFO)
formatter = logging.Formatter("%(levelname)-8s [%(asctime)s] %(name)s[%(funcName)s].%(lineno)d: %(message)s")
ch.setFormatter(formatter)
logger.addHandler(ch)

WEBSOCKET_URL = os.getenv("WEBSOCKET_URL", "ws://consumer_db_to_websocket:8080")
delta_list: list[float] = []
websocket_heartbeat_counter = Counter("websocket_heartbeat", "Amount of heartbeat")
websocket_events_counter = Counter("websocket_events", "Amount of events WebSocket")
amount_of_event_in_separate_camera = Gauge(
    "amount_of_event_in_separate_camera",
    "Amount of events into separate camera",
    ["camera_id"],
)
latency_of_event_in_separate_camera = Gauge(
    "latency_of_event_in_separate_camera",
    "Latency of events into separate camera",
    ["camera_id"],
)

DELTA = 700


def start_consumer_from_websocket_server():
    ws = None
    try:
        ws = create_connection(
            WEBSOCKET_URL,
        )  # TODO set from env var , default is consumer_db_to_websocket for local launch
        logger.info("connected to ws")
        start_datetime = datetime.utcnow()
        delta_list = []
        while True:
            try:
                ws_dict = json.loads(ws.recv())
                timestamp = ws_dict["timestamp"]
                sys_timestamp = time.time() * 1000
                delta = sys_timestamp - timestamp
                message_type = ws_dict["type"]
                if message_type == "event":
                    camera_id = ws_dict["camera_id"]
                    amount_of_event_in_separate_camera.labels(camera_id=camera_id).inc()
                    if delta > DELTA:
                        latency_of_event_in_separate_camera.labels(camera_id=camera_id).inc()
                    CastleMetricsHandler.update_cummulative_average_delay_metric(
                        delta_list=delta_list,
                        delta=delta,
                        metric=CastleMetricsHandler.COMMON_CAD,
                    )
                    CastleMetricsHandler.update_cummulative_average_delay_metric(
                        delta_list=delta_list,
                        delta=delta,
                        metric=CastleMetricsHandler.COMMON_CAD,
                    )
                    CastleMetricsHandler.heartbeat_and_event_web_socket_metric(
                        metric_heartbeat=CastleMetricsHandler.WEB_SOCKET_HEARTBEAT,
                        metric_event=CastleMetricsHandler.WEB_SOCKET_EVENT,
                        heartbeat=1,
                        event=1,
                    )
                    websocket_events_counter.inc()
                    new_datetime = datetime.utcnow()
                    rps_delta_in_seconds = (new_datetime - start_datetime).seconds
                    if rps_delta_in_seconds <= 1:
                        CastleMetricsHandler.EVENTS_PER_SECOND_ON_RECEIVER.inc()
                    else:
                        start_datetime = new_datetime
                        CastleMetricsHandler.EVENTS_PER_SECOND_ON_RECEIVER.set(0)
                else:
                    new_datetime = datetime.utcnow()
                    rps_delta_in_seconds = (new_datetime - start_datetime).seconds
                    if rps_delta_in_seconds > 1:
                        start_datetime = new_datetime
                        CastleMetricsHandler.EVENTS_PER_SECOND_ON_RECEIVER.set(0)
                if message_type == "heartbeat":
                    new_datetime = datetime.utcnow()
                    if rps_delta_in_seconds <= 3:
                        CastleMetricsHandler.HEARTBEAT_PER_SECOND_ON_RECEIVER.inc()
                    else:
                        start_datetime = new_datetime
                        CastleMetricsHandler.HEARTBEAT_PER_SECOND_ON_RECEIVER.set(0)
                    CastleMetricsHandler.heartbeat_and_event_web_socket_metric(
                        metric_heartbeat=CastleMetricsHandler.WEB_SOCKET_HEARTBEAT,
                        metric_event=CastleMetricsHandler.WEB_SOCKET_EVENT,
                        heartbeat=1,
                        event=0,
                    )
                    websocket_heartbeat_counter.inc()

                logger.info(ws_dict)
                logger.info(f"Delta timestamp --- {delta:.0f} ms")
            except WebSocketConnectionClosedException as e:
                logger.info(e)
                logger.info("disconnect ws")
                ws.close()
                CastleMetricsHandler.heartbeat_and_event_web_socket_metric(
                    metric_heartbeat=CastleMetricsHandler.WEB_SOCKET_HEARTBEAT,
                    metric_event=CastleMetricsHandler.WEB_SOCKET_EVENT,
                    heartbeat=0,
                    event=0,
                )
                ws = create_connection(WEBSOCKET_URL)
    except KeyboardInterrupt as e:
        logger.info(e)
        ws.close()
    except Exception:
        CastleMetricsHandler.heartbeat_and_event_web_socket_metric(
            metric_heartbeat=CastleMetricsHandler.WEB_SOCKET_HEARTBEAT,
            metric_event=CastleMetricsHandler.WEB_SOCKET_EVENT,
            heartbeat=0,
            event=0,
        )


if __name__ == "__main__":
    logger.info("starting prom web server...")
    prom.start_http_server(
        8000,
    )
    CastleMetricsHandler.COMMON_CAD.set(0)
    CastleMetricsHandler.EVENTS_PER_SECOND_ON_RECEIVER.set(0)
    logger.info("start consuming ws events")
    while True:
        start_consumer_from_websocket_server()
