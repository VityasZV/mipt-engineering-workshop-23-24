import typing as t
import uuid

from locust import User, constant, events, task

from utils.kafka_sender import KafkaSender
import random


def without_keys(d: dict[t.Hashable, t.Any], keys: t.Iterable[t.Hashable]) -> dict[t.Hashable, t.Any]:
    return {k: d[k] for k in d.keys() - keys}


class KafkaUser(User):
    wait_time = constant(1)

    @task
    def send_data_ml_to_websocket_load(self):
        sender = KafkaSender()
        random_index = random.randint(1, 100) % 2
        camera_type = ["T", "V"]
        camera_name = f"camera_name_{uuid.uuid4()}{camera_type[random_index]}"
        try:
            sender.produce_to_kafka(camera_name)
            events.request.fire(
                request_type="KAFKA_WEBSOCKET",
                name="ml_to_websocket_all_success",
                response_length=0,
                response_time=0,
                context=None,
                exception=None,
            )
        except Exception as e:
            events.request.fire(
                request_type="KAFKA_WEBSOCKET",
                name="ml_to_websocket_all_success",
                response_length=0,
                response_time=0,
                context=None,
                exception=e,
            )
