import json
import logging
import random
from datetime import datetime, timedelta
from functools import partial

from confluent_kafka import Producer

logger = logging.getLogger(__name__)


class KafkaSender:
    producer = Producer({"bootstrap.servers": "localhost:29092"})

    # def __init__(self):
    #     # возможно стоит вынести в переменную класса, могло держаться до 1000 запросов
    #     self.producer = Producer({"bootstrap.servers": "localhost:29092"})

    @staticmethod
    def delivery_report(err, msg: str) -> None:
        if err is None:
            logger.info("Message delivered to {0}:{1} [{2}]".format(msg.topic(), msg.value(), msg.partition()))
            return
        err_message = "Message delivery failed: {0}".format(err)
        logger.error(err_message)
        raise RuntimeError(err_message)

    @staticmethod
    def generate_data(camera_name: str) -> dict:
        str_datetime_now = str(datetime.utcnow())
        str_datetime_yesterday = str(datetime.utcnow() - timedelta(days=1))
        list_dt = [str_datetime_now, str_datetime_yesterday]
        some_minio_url = "some_minio_url"
        dt_to_send = random.choice(list_dt)
        return {
            "topic": "PROCESS_DATA",
            "data": {
                "camera_name": camera_name,
                "event_datetime": dt_to_send,
                "main_image": some_minio_url,
                "ori_main_image": some_minio_url,
                "meta_info": {"cam_name": "rtsp_url"},
                "event_track_id": 13,
                "event_track_duration": 13,
                "event_track_datetime": dt_to_send,
                "objects": [
                    {
                        "image": some_minio_url,
                        "type": "other",
                        "object_id": 1,
                        "duration": 5,
                        "cls_probability": 0.88,
                        "det_probability": 0.88,
                        "meta_info": None,
                        "bbox_coordinates": [0.6, 0.5, 0.6, 0.5],
                        "direction_of_object": 1,
                        "speed_of_object": 1,
                    },
                ],
            },
            "raise_exception": True,
        }

    def produce_to_kafka(self, camera_name) -> None:
        self.producer.poll(0)
        delivery_report_func = partial(self.delivery_report)
        data = self.generate_data(camera_name)
        topic = data.get("topic")
        data = data.get("data")
        self.producer.produce(topic, json.dumps(data).encode(), callback=delivery_report_func)
        self.producer.flush()
