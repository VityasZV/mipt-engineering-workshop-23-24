from locust import HttpUser, task


class TestsServer(HttpUser):
    @task
    def test_admin(self):
        self.client.get("/admin")

    @task
    def test_web_push(self):
        self.client.get("/")

    @task
    def send_push(self):
        test_data = {"id": 1, "body": "hello", "head": "artem"}
        self.client.post("/send_push", json=test_data)
